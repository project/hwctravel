<?php

/**
 * Add custom.css if file exist.
 */
$custom_css = path_to_theme() .'/custom.css';
if (file_exists($custom_css)) {
  drupal_add_css($custom_css, 'theme');
}

function phptemplate_body_class($sidebar_right) {
  if ($sidebar_right != '') {
    $class = 'sidebar-right';
  }

  return $class ? $class : 'sidebar-right';
}

/**
 * Return a themed mission trail.
 *
 * @return
 *   a string containing the mission output, or execute PHP code snippet if
 *   mission is enclosed with <?php ?>.
 */
function hwctravel_mission() {
  $mission = theme_get_setting('mission');
  if (preg_match('/^<\?php/', $mission)) {
    $mission = drupal_eval($mission);
  }
  else {
    $mission = filter_xss_admin($mission);
  }
  return isset($mission) ? $mission : '';
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode('', $breadcrumb) .'</div>';
  }
}

/**
 * Return a cascade primary links.
 * Clone implementation from user_block().
 *
 * @return
 *   a themed cascade primary links.
 */
function phptemplate_get_primary_links() {
  return theme('menu_tree', variable_get('menu_primary_menu', 0));
}
