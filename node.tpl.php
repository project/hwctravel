<?php
?>
<div class="node<?php print $sticky ? ' sticky' : '' ?><?php print $status ? '' : ' node-unpublished' ?>">
  <?php if ($page == 0): ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title ?></a></h2><?php endif; ?>
  <div class="meta meta-header">
    <?php if ($picture): print $picture; endif; ?>
    <?php if ($submitted): ?><div class="submitted"><?php print $submitted ?></div><?php endif; ?>
    <?php if ($terms): ?><div class="terms"><?php print $terms ?></div><?php endif; ?>
  </div>
  <div class="content">
    <?php print $content?>
    <div class="meta meta-footer">
      <?php if ($links): ?><?php print $links ?><?php endif; ?>
    </div>
  </div>
</div>
