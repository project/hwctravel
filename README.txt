
ABOUT HWCTRAVEL
----------------------

This is a re-implementation of the Multiflex-3 theme by Gerhard. This is
a tableless, 2 column, fixed width layout. Parts of the design are
ported from Contented7, Internet Services and Interactive Media theme.

Official HWCTRAVEL PAGE
  http://www.hostwaves.com/design-sample/travel-1


LIST OF MAINTAINERS
----------------------

PROJECT OWNER
M: MOHD KHALEMI <mkhalemi@hostwaves.com>
W: http://www.hostwaves.com/

M: WIZAN ZAINI <wizan@hostwaves.com>
W: http://www.hostwaves.com/
