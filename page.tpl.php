<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">

<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if lt IE 7]>
    <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/fix-ie.css";</style>
  <![endif]-->
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body class="<?php print phptemplate_body_class($sidebar_right) ?>">
<div id="header-region" class="clear-block"><?php print $header ?></div>
<div id="wrapper"><!-- begin wrapper -->
<div id="container" class="clear-block"><!-- begin container -->
  <div id="header"><!-- begin header -->
    <?php if ($logo): ?><div id="logo"><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><img src="<?php print $logo ?>" alt="<?php print $site_name ?>" /></a></div><?php endif; ?>
    <div id="slogan-floater"><!-- begin slogan-floater -->
      <?php if ($site_name): ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h1><?php endif; ?>
      <?php if ($site_slogan): ?><div class='site-slogan'><?php print $site_slogan ?></div><?php endif; ?>
    </div><!-- end slogan-floater -->
    <?php if (isset($secondary_links)) { ?>
      <div id="secondary-links"><!-- start secondary-links -->
        <?php print theme('links', $secondary_links) ?>
      </div><!-- end secondary-links -->
    <?php } ?>
  </div><!-- end header -->
  
  <?php if (isset($primary_links)) { ?>
    <div id="primary-links"><!-- start primary-links -->
      <?php print phptemplate_get_primary_links() ?>
    </div><!-- end primary-links -->
  <?php } ?>
  
  

  <div id="main"><div class="right-corner"><div class="left-corner">
  
  <!-- begin main -->
  
  
  <div id="center"><div id="squeeze"><!-- begin center -->


    <?php if ($title): print '<h2 class="title'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif; ?>
    <?php if ($tabs): print '<div class="tabs">'. $tabs .'</div>'; endif; ?>
    <?php if ($messages): print $messages; endif; ?>
    <?php print $help ?>
    <div class="clear-block">
      <?php print $content ?>
    </div>
    
	<div id="breadcrumb-search">
    <?php if ($mission): print '<div id="mission">'. theme('mission') .'</div>'; endif; ?>
    <?php print $breadcrumb ?>
    <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
  </div>

  </div></div><!-- end center -->
  <?php if ($sidebar_right) { ?>
    <div id="sidebar-right" class="sidebar"><!-- begin sidebar-right -->
      <?php print $sidebar_right ?>
    </div><!-- end sidebar-right -->
  <?php } ?>
  </div></div></div><!-- end main -->
  <div id="footer"><!-- start footer -->
    <?php print $footer_message ?><p>Theme by Hostwaves of Randuk Benua</p>
    <!-- begin #287426 -->
      <span style="display: none;">&nbsp;</span>
    <!-- end #287426 -->
  </div><!-- end footer -->
</div><!-- end wrapper -->
</div><!-- end container -->
<?php print $closure ?>
</body>
</html>
